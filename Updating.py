# Created by Keegan for Reborn's Christmas celebration
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# Reborn OS Discord: keeganmilsten
# Reborn OS Forum: keeganmilsten

# This ensures that the Gtk version is 3.0
import subprocess
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from os.path import expanduser
home = expanduser("~")
print(home)
# os.environ['HOME']
# Create Handlers (Triggers) for each item

class Handler:
# Close the window
    def onDestroy(self, *args):
        Gtk.main_quit()

################################################################################
############################### First Tab ######################################
################################################################################

    # Open Article
    def goArticle(self, button):
        subprocess.Popen(['xdg-open', 'https://reborn-os.weebly.com/archived-posts/100-off-and-more-exciting-news'])

    # Install/Launch BlockSK8
    def goBlockSK8(self, button):
        subprocess.Popen(['bash', '/usr/bin/installer.sh'])

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(home + '/.local/share/Reborn/RebornMaintenance2.glade')
builder.connect_signals(Handler())

window2 = builder.get_object("RebornChristmas")
window2.show_all()

Gtk.main()
