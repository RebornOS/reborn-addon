function Remove() {
    rm -f $HOME/.local/share/Reborn/RebornMaintenance2.glade
    rm -f $HOME/.local/share/applications/RebornUpdates.desktop
    rm -f $HOME/.local/share/Reborn/Updating.py
    rm -f $HOME/.local/share/Reborn/presents.png
    rm -f /etc/xdg/autostart/RebornUpdating.desktop
    rm -f /usr/local/lib/RebornUpdates.desktop
    rm -f /usr/local/lib/Updating.py 
    rm -f /usr/local/lib/presents.png
    rm -f /usr/local/lib/RebornMaintenance2.glade
    rm -f /usr/bin/BlockSK8-installer.sh
    rm -rf $HOME/.local/share/Reborn
}

export -f Remove

if [[ $(date +%-j) -ge 359 && $(date +%-j) -le 365 ]]; then
    mkdir 777 $HOME/.local/share/Reborn
    cp /usr/local/lib/RebornMaintenance2.glade $HOME/.local/share/Reborn/
    cp /usr/local/lib/RebornUpdates.desktop $HOME/.local/share/applications/
    cp /usr/local/lib/Updating.py $HOME/.local/share/Reborn/
    cp /usr/local/lib/presents.png $HOME/.local/share/Reborn/
    python3 $HOME/.local/share/Reborn/Updating.py

elif [[ $(date +%-j) -le 30 && $(date +%-j) -ge 0 ]]; then
    yadsu Remove
fi
