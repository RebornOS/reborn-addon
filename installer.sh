#!/bin/bash

#Written for Reborn OS. All code and artwork was done by Caden Mitchell (TriVoxel.)
#https://TriVoxel.weebly.com | https://reborn-os.weebly.com | https://blocksk8.page.link/git
creds=$(yad --center \
    --width=350 \
    --height=100 \
    --form \
    --title="Root" \
    --save \
    --text="Please enter your root password:" \
	--field="$ROOT_PASS":H)

export PASSWORD=${creds%|*}

killall zenity
if [[ -f /bin/blocksk8 ]]; then
    echo $PASSWORD | killall python3
    echo $PASSWORD | killall python
    bash blocksk8
else
    echo $PASSWORD | sudo pacman -S zenity python python3 mesa mesa-demos --noconfirm --needed
    echo $PASSWORD | killall zenity
    zenity --info --text="BlockSk8 installer is currently fetching files. This operation could take some time, but will close when done." --ellipsize --title="BlockSk8 Installer" --window-icon=./blocksk8.png & echo "Removing old installer files..." ; fg
    rm -rvf /tmp/blocksk8/
    echo "Downloading game files..."
    echo $PASSWORD | git clone -b linux https://gitlab.com/TriVoxel/blocksk8.git /tmp/blocksk8/
    echo $PASSWORD | chmod +x /tmp/blocksk8/askpass.sh
    echo "Download complete."

    export SUDO_ASKPASS="/tmp/blocksk8/askpass.sh"

    install='/usr/share/blocksk8'

#    (( EUID != 0 )) && exec sudo -A -- "$0" "$@"

    echo $PASSWORD | killall zenity
    zenity --info --text="BlockSk8 is currently installing. This will close when the operation is completed." --ellipsize --title="BlockSk8 Installer" --window-icon=./blocksk8.png & echo "Beginning installation..." ; fg
    echo "--------------------------------"
    echo "++++++++++++++++++++++++++++++++"
    echo "--------------------------------"
    echo "Creating directories..."
    echo $PASSWORD | mkdir -v $install
    echo "Installing game files..."
    echo $PASSWORD | cp -rv /tmp/blocksk8/BlockSk8-html5-intellij/ $install
    echo $PASSWORD | cp -v /tmp/blocksk8/BlockSk8-html5.hxproj $install
    echo $PASSWORD | cp -v /tmp/blocksk8/blocksk8 /bin/
    echo $PASSWORD | cp -v /tmp/blocksk8/blocksk8.desktop /usr/share/applications/
    echo $PASSWORD | cp -v /tmp/blocksk8/blocksk8.png $install
    echo $PASSWORD | cp -rv /tmp/blocksk8/compiled/ $install
    echo $PASSWORD | cp -rv /tmp/blocksk8/html5/ $install
    echo $PASSWORD | cp -rv /tmp/blocksk8/html5-resources/ $install
    echo $PASSWORD | cp -v /tmp/blocksk8/project-html5.hxml $install
    echo $PASSWORD | cp -rv /tmp/blocksk8/temp/ $install
    echo "Installing updater..."
    echo $PASSWORD | cp -v /tmp/blocksk8/blocksk8-updater /bin/
    echo $PASSWORD | cp -v /tmp/blocksk8/blocksk8-updater.desktop /usr/share/applications/
    echo $PASSWORD | cp -v /tmp/blocksk8/askpass.sh $install
    echo "Changing permissions of game files..."
    echo $PASSWORD | chmod -v +x /bin/blocksk8
    echo $PASSWORD | chmod -v +x /bin/blocksk8-updater
    echo $PASSWORD | chmod -v +x /usr/share/applications/blocksk8.desktop
    echo $PASSWORD | chmod -v +x /usr/share/applicatios/blocksk8-updater.desktop
    echo $PASSWORD | chmod -v +x /usr/share/blocksk8/askpass.sh
    echo "Linking scripts..."
    echo $PASSWORD | cp -v /bin/blocksk8 /usr/bin/
    echo $PASSWORD | cp -v /bin/blocksk8-updater /usr/bin/blocksk8-updater/
    echo $PASSWORD | killall zenity
    zenity --info --text="The BlockSk8 installer has finished. You may exit the program." --ellipsize --title="BlockSk8 was Installed" --window-icon=./blocksk8.png
    echo "Updater completed."
    echo "Installer completed."
    echo "Run the command 'blocksk8' or 'sh /bin/blocksk8' to play."
	echo $PASSWORD | killall python3
	echo $PASSWORD | killall python
	bash blocksk8
fi
